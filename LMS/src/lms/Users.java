/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lms;

import java.awt.Font;
import java.util.*;
import javax.swing.UIManager;
/**
 *
 * @author rishabagarwal
 */
/*  Where you want to use the users add these line

import static lms.Users.scientist;
import static lms.Users.intern;
import static lms.Users.buyer;
import static lms.Users.seller;
import static lms.Users.product;
import static lms.Users.transaction;

*/




public class Users {

   public static ArrayList<Scientist> scientist = new ArrayList<Scientist>();
   public static ArrayList<Intern> intern = new ArrayList<Intern>();
   public static ArrayList<Buyer> buyer = new ArrayList<Buyer>();
   public static ArrayList<Seller> seller = new ArrayList<Seller>();
   public static ArrayList<Product> products =  new ArrayList<Product>();
   public static ArrayList<Transaction> transaction =  new ArrayList<Transaction>();
   

   public static void main(String args[]){
       //UIManager.getLookAndFeelDefaults().put("defaultFont", new Font("Dialog",Font.PLAIN, 16));
       Scientist sc1 = new Scientist("rishab","password","Rishab",true);
       scientist.add(sc1);
       Scientist sc2 = new Scientist("sankalp","password","Sankalp",true);
       scientist.add(sc2);
       Scientist sc3 = new Scientist("krish","password","Krish",true);
       scientist.add(sc3);
       Scientist sc4 = new Scientist("rutanjit","password","Rutanjit",true);
       scientist.add(sc4);
       Scientist sc5 = new Scientist("newton","password","Newton",true);
       scientist.add(sc5);

       Intern in1 = new Intern ("diana","password","Diana",true);//lol
       intern.add(in1);
       Intern in2 = new Intern ("raj","password","Raj",true);
       intern.add(in2);
       Intern in3 = new Intern ("einstein","password","Einstein",true);
       intern.add(in3);
       Intern in4 = new Intern ("elizabeth","password","Elizabeth",true);
       intern.add(in4);
       Intern in5 = new Intern ("osama911","password","Osama",true);
       intern.add(in5);


       Buyer buy1 = new Buyer("muskaan","password","Muskaan", true);
       buyer.add(buy1);
       Buyer buy2 = new Buyer("hafeez","password","Hafeez", true);
       buyer.add(buy2);
       Buyer buy3 = new Buyer("ujwal","password","Ujwal", true);
       buyer.add(buy3);
       Buyer buy4 = new Buyer("prerna","password","Prerna", true);
       buyer.add(buy4);
       Buyer buy5= new Buyer("rajesh","password","Rajesh", true);
       buyer.add(buy5);


       Seller sel1 =  new Seller("qmchem","password","QM Chemicals", true);
       seller.add(sel1);
       Seller sel2 =  new Seller("pankla","password","Pankaj Labs", true);
       seller.add(sel2);
       Seller sel3 =  new Seller("boro","password","Borosil", true);
       seller.add(sel3);
       Seller sel4 =  new Seller("deltchem","password","Delta Chem", true);
       seller.add(sel4);
       Seller sel5 =  new Seller("domsup","password","Dominoes Supplies", true);
       seller.add(sel5);

       Product pro1 = new Product(1,"NaCl",649, 89,false);
       products.add(pro1);
       Product pro2 = new Product(2,"H2SO4",1299, 100,false);
       products.add(pro2);
       Product pro3 = new Product(3,"Beaker",149, 55,false);
       products.add(pro3);
       Product pro4 = new Product(4,"Testube",99, 500,false);
       products.add(pro4);
       Product pro5 = new Product(5,"CuSO4",999, 60,false);
       products.add(pro5);
       Product pro6 = new Product(6,"CH3COOH",399, 110,false);
       products.add(pro6);
       Product pro7 = new Product(7,"HCl",649, 100,false);
       products.add(pro7);
       Product pro8 = new Product(8,"NaOH",899, 35,false);
       products.add(pro8);
       Product pro9 = new Product(9,"Mg",1049, 40,false);
       products.add(pro9);
       Product pro10 = new Product(10,"CaCO3",749, 30,false);
       products.add(pro10);
       Product pro11 = new Product(11,"Burner",1499, 20,false);
       products.add(pro11);
       Product pro12 = new Product(12,"Pippette",149, 327,false);
       products.add(pro12);
       Product pro13 = new Product(13,"Burette",499, 113,false);
       products.add(pro13);
       Product pro14 = new Product(14,"Distilled Water",249, 150,false);
       products.add(pro14);
       
       
       Product p = new Product();
       
       Transaction t =  new Transaction();
       t.buyer="muskaan";
       t.seller="pankla";
       t.id=1;
       t.p[0][0]= 5;
       t.p[0][1] = 3;
       t.p[1][0]=13;
       t.p[1][1]=15;
       transaction.add(t);
       t.buyer="muskaan";
       t.seller="boro";
       t.id=2;
       t.p[0][0]= 3;
       t.p[0][1] = 50;
       t.p[1][0]=6;
       t.p[1][1]=7;
       transaction.add(t);
       t.buyer="ujwal";
       t.seller="boro";
       t.id=3;
       t.p[0][0]= 8;
       t.p[0][1] = 6;
       transaction.add(t);
       t.buyer="rajesh";
       t.seller="qmchem";
       t.id=4;
       t.p[0][0]= 11;
       t.p[0][1] = 3;
       transaction.add(t);
       t.buyer="muskaan";
       t.seller="pankla";
       t.id=1;
       t.p[0][0]= 5;
       t.p[0][1] = 3;
       t.p[1][0]=13;
       t.p[1][1]=15;
       transaction.add(t);

      new LoginActivity().setVisible(true);
//        new PreviousOrderActivity("muskaan").setVisible(true);
       //System.out.println("Hello World");
   }
}
class Scientist{
    String username;
    String password;
    String name;
    boolean valid = false;

    Scientist(String username, String password, String name, boolean valid){
        this.username = username;
        this.password = password;
        this.name = name;
        this.valid = valid;
    }
    Scientist(){
        this.username = "";
        this.password ="";
        this.name = "";
    }
 }

class Intern{
    String username;
    String password;
    String name;
    boolean valid = false;

    Intern(String username, String password, String name, boolean valid){
        this.username = username;
        this.password = password;
        this.name = name;
        this.valid = valid;
    }
    Intern(){
        this.username = "";
        this.password = "";
        this.name = "";
    }

 }

class Buyer{
    String username;
    String password;
    String name;
    boolean valid = false;

    Buyer(String username, String password, String name, boolean valid){
        this.username = username;
        this.password = password;
        this.name = name;
        this.valid = valid;
    }
    Buyer(){
        this.username = "";
        this.password ="";
        this.name = "";
    }
}

class Seller{
    String username;
    String password;
    String name;
    boolean valid = false;
    ArrayList<Product> product =  new ArrayList<Product>();

    Seller(String username, String password, String name, boolean valid){
        this.username = username;
        this.password = password;
        this.name = name;
        this.valid = valid;
    }
    Seller(){
        this.username = "";
        this.password ="";
        this.name = "";
    }
 }

class Product{
    int pid;
    String name;
    float cost;
    int last_ordered;
    int search_no;
    int current_stock;
    int pre_order;
    boolean dynamic_pricing ;

    Product(int pid, String name, float cost, int current_stock, boolean dynamic_pricing){
        this.pid = pid;
        this.name = name;
        this.cost = cost;
        this.current_stock = current_stock;
        this.dynamic_pricing = dynamic_pricing;
        this.pre_order = 0 ;
        this.search_no = 0;
        this.last_ordered = 0;
    }
    Product(){
        this.pid = 0;
        this.name = "";
        this.cost = 0;
        this.current_stock = 0;
        this.dynamic_pricing = false;
        this.pre_order = 0 ;
        this.search_no = 0;
        this.last_ordered = 0;
    }
    void update_stock(int n){
        this.current_stock = this.current_stock + n;
    }
    void update_price(){
        //TODO: Rishab Agarwal
    }

}

class Transaction{
    int id;
    String buyer;
    String seller;
    int[][] p = new int[10][2];
    int total;
}
